package com.epam.stv.factory.factorypages;

//import com.epam.stv.com.epam.stv.design.WiggleSignInPage;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Tatiana_Sauchanka on 3/6/2017.
 */
public class WiggleMainFactoryPage extends FactoryPage {


    @FindBy (css = "#btnSignIn")
    private WebElement signLink;

    @FindBy (id = "btnJoinLink")
    private WebElement registerLink;

    @FindBy (css = "a[href='http://www.wiggle.com/cycle/']")
    private WebElement cycleTab;

    public WiggleMainFactoryPage(WebDriver driver){
        super(driver);
    }

    public WiggleSignInFactoryPage clickOnSignLink() {
        System.out.println("Go to Sign in page");
        signLink.click();
        return new WiggleSignInFactoryPage(driver);
    }

    public WiggleSignInFactoryPage clickOnSignLinkViaJs() {
        System.out.println("Click on Sign Link Via js");
        JavascriptExecutor jsExec = (JavascriptExecutor) driver;
        jsExec.executeScript("document.getElementById('btnSignIn').click()");
        return new WiggleSignInFactoryPage(driver);
    }

    public WiggleRegisterFactoryPage clickOnRegisterLink() {
        System.out.println("Go to Register page by js clicking on Register link");
        JavascriptExecutor jsExec = (JavascriptExecutor) driver;
        jsExec.executeScript("document.getElementById('btnJoinLink').click()");
        return new WiggleRegisterFactoryPage(driver);
    }

    public boolean isRegisterLinkDisplayed(){
        return registerLink.isDisplayed();
    }

    public boolean isSignLinkDisplayed(){
        return signLink.isDisplayed();
    }

    public WiggleMainFactoryPage moveToOverCycleTab() {
        System.out.println("Mouse over Cycle item from the menu using Actions");
        new Actions(driver).moveToElement(cycleTab).build().perform();
        return new WiggleMainFactoryPage(driver);
    }

    public boolean isCycleSubmenuDisplayed(){
//        WebDriverWait wait = new WebDriverWait(driver, 10);

//        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("someid")));
        WebElement dynamicElement = (new WebDriverWait(driver, 30))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@data-page-area='MegaMenu']/li[2]")));
        return dynamicElement.isDisplayed();
    }




}
