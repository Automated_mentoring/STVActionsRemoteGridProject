package com.epam.stv.factory.factorypages;

import com.epam.stv.loginpage.LoginPageValidater;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;


/**
 * Created by Tatiana_Sauchanka on 3/6/2017.
 */
public class WiggleSignInFactoryPage extends FactoryPage {
    @FindBy (xpath = "//input[@name='LogOnModel.UserName']")
    private WebElement emailAddress;

    @FindBy (css = "#passwordToggle")
    private WebElement showPassword;

    @FindBy (xpath = "//input[@name='LogOnModel.Password']")
    private WebElement passwordField;

    @FindBy (id = "qa-login")
    private WebElement loginButton;

    @FindBy (css = ".bem-alert--danger")
    private WebElement warningMessage;

    @FindBy (css = ".bem-checkout__logo")
    private WebElement orangeIcon;

    @FindBy (id = "qa-dual-register")
    private WebElement registerButton;

    @FindBy (css = "span[for='DualRegisterEmailModel_Email']")
    private WebElement validationError;

    public WiggleSignInFactoryPage(WebDriver driver){
        super(driver);
    }

    public WiggleSignInFactoryPage pressRegisterButton() {
        System.out.println("Press Register button via Actions click");
        new Actions(driver).click(registerButton).perform();
        return this;
    }

    public WiggleSignInFactoryPage inputEmailAddress(String userEmail) {
        emailAddress.isDisplayed();
        emailAddress.sendKeys(userEmail);
        return this;
    }

    public WiggleSignInFactoryPage clickOnShowPassword() {
        System.out.println("Inspect Password chars");
        new Actions(driver).click(showPassword).perform();
        return this;
    }

    public WiggleSignInFactoryPage inputPassword(String password) {
        passwordField.isDisplayed();
        passwordField.sendKeys(password);
        return this;
    }

    public WiggleMainFactoryPage clickLoginButton() {
        System.out.println("Press login button via Enter key");
        loginButton.isDisplayed();
        new Actions(driver).sendKeys(loginButton, Keys.ENTER).perform();
        return new WiggleMainFactoryPage(driver);
    }

    public WiggleMainFactoryPage clickOnOrangeWiggleIcon() {
        System.out.println("Navigate to main page");
        orangeIcon.click();
        return new WiggleMainFactoryPage(driver);
    }

    public boolean isEmailAddressFieldDisplayed(){
        return emailAddress.isDisplayed();
    }

    public boolean isWiggleIconDisplayed(){
        return orangeIcon.isDisplayed();
    }

    public boolean isLoginButtonDisplayed(){
        return loginButton.isDisplayed();
    }

    public boolean isRegisterButtonDisplayed(){
        return registerButton.isDisplayed();
    }

    public boolean isWarningMessageDisplayed(){
        return warningMessage.isDisplayed();
    }

    public String warningMessageText() {
        String warningText = warningMessage.getText().substring(0, 30);
        return warningText;
    }

    public String lableText() {
        String loginText = loginButton.getText();
        return loginText;
    }

    public boolean isValidationErrorDisplayed(){
        return validationError.isDisplayed();
    }

    public WiggleSignInFactoryPage highlightRegisterButton() {
        LoginPageValidater highlightedElement = new LoginPageValidater();
        highlightedElement.highlightElement(driver, registerButton);
        return this;
    }


}
